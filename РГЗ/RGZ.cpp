#include <iostream>
#include "mpi.h"
#include <chrono>
#include <vector>
#include <cmath>

using namespace std;

int sizeProc = 0;//Количество процессов
int rankProc = 0;//Номер процесса
const int s = 100000;//Размер массива для сортировки

int def_t(int N){
	double h = log2(N);
	return ceil(h);
}

int main(int argc, char**argv){
	unsigned p, d, q, r = 0, t;
	int first_size;
	double startwtime = 0.0, endwtime;
	vector<int> mas;
	//Инициализуем массив для сортировки
	for(int i=0; i < s; i++)
		mas.push_back(rand()%10000+1);
		
	first_size = mas.size();
	//Если размер массива не кратен 2 в какой-то степени, то дополняем нулями, которых в массиве быть не может
	t = def_t(s);
	p = pow(2,t-1);	
	if (mas.size()!=2*p)
		while(mas.size()!=2*p)
			mas.push_back(0);
	
			
	int k1 = 1, k2, h;
	int N;
	MPI_Init(&argc, &argv);//Инициализация

	MPI_Comm_size(MPI_COMM_WORLD, &sizeProc); //Записываем количество процессов    
	MPI_Comm_rank(MPI_COMM_WORLD, &rankProc); //Записываем номер текущего процесса
	cout << "RankProc: " << rankProc << endl;//Количество потоков должно быть кратно размеру массиву с 0
	
	//Определим значения p,d,t,r алгоритма
	t = def_t(s);
	p = pow(2,t-1);
	d = p;
	q = pow(2,t-1);
	
	int help;
	startwtime = MPI_Wtime();
	while(k1){
		k2 = 1;
		do{
			for (int i=0; i < mas.size()-d; i++){
				if (((i&p) == r) and (((i+1)%(rankProc+1))==0)){
					if(mas[i] > mas[i + d]){
						h = mas[i];
						mas[i] = mas[i + d];
						mas[i + d] = h;
					}
				}
			}
			MPI_Barrier(MPI_COMM_WORLD);
			
				if (q!=p){
					d = q-p;
					q = q/2;
					r = p;
				}else
					k2=0;
			
		}while(k2);
		//MPI_Barrier(MPI_COMM_WORLD);
		
		p = p/2;
		q = pow(2,t-1);
		r = 0;
		d = p;
		if (p<=0)
			k1 = 0;
	}
	endwtime = MPI_Wtime();
    
	MPI_Finalize();
	
	//Удаляем вспомогательные нули
	while (mas[0] == 0){
		mas.erase(mas.begin());
	}
	
	if(rankProc ==0){
		for (int i=0;i < mas.size(); i++)
			cout << i+1 << ")" << mas[i] << endl;
		cout << "Время работы программы:" << endwtime - startwtime << endl;
	}
	return 0;
}
